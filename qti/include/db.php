<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @author Adam Clarke
 * @version 1.0
 * @copyright Copyright (c) 2014 The University of Nottingham
 * @package
 */

require_once dirname(__DIR__, 2) . DIRECTORY_SEPARATOR . 'include' . DIRECTORY_SEPARATOR . 'errors.php';

class Database
{
    public $type = 'SELECT';
    public $table = '';
    public $table_alias = '';

    public $fields = array(); // only for SELECT
    public $leftjoins = array(); // only for SELECT
    public $innerjoins = array(); // only for SELECT
    public $wheres = array(); // only for SELECT/UPDATE
    public $orders = array(); // only for SELECT
    public $values = array(); // only for INSERT/REPLACE
    public $groups = array(); // only for SELECT
    public $set = array(); // only for UPDATE
    public $limit = 0;
    public $limitoffset = 0;
    public $rawquery = ''; // only for RAW

    public $query;

    public $blankrows = array();

    public function DoError($error)
    {
        echo "<font color='red'>$error</font><br />";
    }

    public function SetType($type)
    {
        $this->type = $type;
    }

    public function SetTable($table, $alias = '')
    {
        $this->table = $table;
        $this->table_alias = $alias;

        $this->fields = array(); // only for SELECT
        $this->leftjoins = array(); // only for SELECT
        $this->innerjoins = array(); // only for SELECT
        $this->wheres = array(); // only for SELECT/UPDATE
        $this->orders = array(); // only for SELECT
        $this->values = array(); // only for INSERT/REPLACE
        $this->groups = array(); // only for SELECT
        $this->set = array(); // only for UPDATE
    }

    public function AddField($fieldname)
    {
        if ($this->type != 'SELECT') {
            return $this->DoError('Can only add output field when in SELECT mode');
        }

        if (is_array($fieldname)) {
            foreach ($fieldname as $field) {
                $this->fields[] = $field;
            }
        } else {
            $this->fields[] = $fieldname;
        }
        return '';
    }

    public function AddLeftJoin($table, $alias, $sourcekey, $destkey)
    {
        if ($this->type != 'SELECT') {
            return $this->DoError('Can only add LEFT JOIN when in SELECT mode');
        }

        if ($this->table_alias == '') {
            return $this->DoError('You must have a table alias for the main table to do a LEFT JOIN');
        }

        $join = array();
        $join['table'] = $table;
        $join['alias'] = $alias;
        $join['sk'] = $sourcekey;
        $join['dk'] = $destkey;

        $this->leftjoins[] = $join;
        return '';
    }

    public function AddInnerJoin($table, $alias, $sourcekey, $destkey)
    {
        if ($this->type != 'SELECT') {
            return $this->DoError('Can only add INNER JOIN when in SELECT mode');
        }

        if ($this->table_alias == '') {
            return $this->DoError('You must have a table alias for the main table to do an INNER JOIN');
        }

        $join = array();
        $join['table'] = $table;
        $join['alias'] = $alias;
        $join['sk'] = $sourcekey;
        $join['dk'] = $destkey;

        $this->innerjoins[] = $join;
        return '';
    }

    public function AddWhere($field, $value, $type, $wheretype = '=')
    {
        if ($this->type != 'SELECT' && $this->type != 'UPDATE') {
            return $this->DoError('Can only add WHERE when in SELECT or UPDATE mode');
        }

        $where = array();
        $where['field'] = $field;
        $where['value'] = $value;
        $where['type'] = $type;
        $where['wheretype'] = $wheretype;

        $this->wheres[] = $where;
        return '';
    }

    public function AddOrder($field, $asc = '')
    {
        if ($this->type != 'SELECT') {
            return $this->DoError('Can only add ORDER BY when in SELECT mode');
        }

        $order = array();
        $order['field'] = $field;
        $order['dir'] = $asc;

        $this->orders[] = $order;
        return '';
    }

    public function AddValue($field, $value, $type)
    {
        if ($this->type != 'INSERT' && $this->type != 'REPLACE') {
            return $this->DoError('Can only add field to set when in INSERT or REPLACE mode');
        }

        $valuea = array();
        $valuea['field'] = $field;
        $valuea['value'] = $value;
        $valuea['type'] = $type;

        $this->values[] = $valuea;
        return '';
    }

    public function AddGroup($field)
    {
        if ($this->type != 'SELECT') {
            return $this->DoError('Can only add GROUP BY when in SELECT mode');
        }

        $this->groups[] = $field;
        return '';
    }

    public function AddLimit($limit, $offset = 0)
    {
        if ($this->type != 'SELECT' && $this->type != 'UPDATE') {
            return $this->DoError('Can only add LIMIT when in SELECT or UPDATE mode');
        }

        $this->limit = $limit;
        $this->limitoffset = $offset;
        return '';
    }

    public function AddSet($field, $value, $type)
    {
        if ($this->type != 'UPDATE') {
            return $this->DoError('Can only add SET when in UPDATE mode');
        }

        $set = array();

        $set['field'] = $field;
        $set['value'] = $value;
        $set['type'] = $type;

        $this->sets[] = $set;
        return '';
    }

    public function BuildQuery()
    {
        if ($this->type == 'SELECT') {
            $this->BuildQuerySelect();
        }
        if ($this->type == 'UPDATE') {
            $this->BuildQueryUpdate();
        }
        if ($this->type == 'INSERT') {
            $this->BuildQueryInsert();
        }
        if ($this->type == 'REPLACE') {
            $this->BuildQueryReplace();
        }

        //echo "<br>" . $this->query . "<br>";
    }

    public function BuildQuerySelect()
    {
        $qry = 'SELECT ';

        $qry .= implode(',', $this->fields);

        $qry .= ' FROM ';
        $qry .= $this->table;
        if ($this->table_alias != '') {
            $qry .= ' AS ' . $this->table_alias;
        }

        foreach ($this->leftjoins as $leftjoin) {
            $qry .= sprintf(' LEFT JOIN %s AS %s ON %s.%s = %s.%s ', $leftjoin['table'], $leftjoin['alias'], $this->table_alias, $leftjoin['sk'], $leftjoin['alias'], $leftjoin['dk']);
        }

        foreach ($this->innerjoins as $innerjoin) {
            $qry .= sprintf(' INNER JOIN %s AS %s ON %s.%s = %s.%s ', $innerjoin['table'], $innerjoin['alias'], $this->table_alias, $innerjoin['sk'], $innerjoin['alias'], $innerjoin['dk']);
        }

        $qry .= $this->BuildWhere();

        if (count($this->groups) > 0) {
            $qry .= ' GROUP BY ' . implode(', ', $this->groups);
        }

        if (count($this->orders) > 0) {
            $qry .= ' ORDER BY ';

            $orderbits = array();

            foreach ($this->orders as $order) {
                $orderbits[] = $order['field'] . ' ' . $order['dir'];
            }

            $qry .= implode(', ', $orderbits);
        }

        if ($this->limit > 0) {
            $qry .= ' LIMIT ' . $this->limit;
            if ($this->limit_offset > 0) {
                $qry .= ', ' . $this->limit_offset;
            }
        }

        $this->query = $qry;
    }

    public function BuildWhere()
    {
        $qry = '';

        if (count($this->wheres) > 0) {
            $qry .= ' WHERE ';

            $wherelist = array();

            foreach ($this->wheres as $where) {
                if ($where['wheretype'] == '=') {
                    $wherelist[] = sprintf('%s = ?', $where['field']);
                } elseif ($where['wheretype'] == 'IN') {
                    $valuecount = count($where['value']);
                    foreach ($where['value'] as $value) {
                        $valuelist[] = '?';
                    }
                    $wherelist[] = sprintf('%s IN (%s)', $where['field'], implode(', ', $valuelist));
                }
            }

            $qry .= implode(' AND ', $wherelist);
        }

        return $qry;
    }

    public function BuildQueryUpdate()
    {
    }

    public function BuildQueryInsert()
    {
    }

    public function BuildQueryReplace()
    {
    }

    public function SetRawQuery($qry)
    {
        $this->rawquery = $qry;
    }

    public function BindWhere($stmt)
    {
        if (count($this->wheres) == 0) {
            return;
        }

        $params = array();
        $params[0] = $stmt;
        $params[1] = '';

        for ($i = 0; $i < count($this->wheres); $i++) {
            $where = $this->wheres[$i];

            if ($where['wheretype'] == '=') {
                $params[1] .= $where['type'];

                // Params to mysqli_stmt_bind_param now need to be reference
                $params[] =& $this->wheres[$i]['value'];

                // $params[] = &$where['value'];
            } elseif ($where['wheretype'] == 'IN') {
                foreach ($where['value'] as $value) {
                    $params[1] .= $where['type'];
                    $params[] = $value;
                }
            }
        }
        /*      foreach ($this->wheres as $where)
        {
        if ($where['wheretype'] == "=")
        {
        $params[1] .= $where['type'];

        // Params to mysqli_stmt_bind_param now need to be reference
        //$params[] = &($this->wheres[$where]['value']);

        $params[] = &$where['value'];
        } else if ($where['wheretype'] == "IN")
        {
        foreach ($where['value'] as $value)
        {
        $params[1] .= $where['type'];
        $params[] = $value;
        }
        }
        }
        */

        @call_user_func_array('mysqli_stmt_bind_param', $params);
    }

    public function BindSet()
    {
    }

    public function BindInsert()
    {
    }

    public function Execute()
    {
    }

    public function GetSingleRow()
    {
        global $mysqli;

        $this->BuildQuery();

        //echo "QRY : " . $this->query . "<BR>";
        $stmt = $mysqli->prepare($this->query);

        $this->BindWhere($stmt);

        $stmt->execute();

        $data = mysqli_stmt_result_metadata($stmt);

        $fields = array();
        $out = array();

        $fields[0] = $stmt;

        while ($field = mysqli_fetch_field($data)) {
            $fields[] =& $out[$field->name];
        }

        call_user_func_array('mysqli_stmt_bind_result', $fields);

        if ($stmt->fetch()) {
            return $out;
        } else {
            $this->DoError('GetSingleRow::Error in query : ' . $this->query . '<br>');
        }
    }

    public function GetMultiRow()
    {
        global $mysqli;

        $this->BuildQuery();

        //echo "QRY : " . $this->query . "<BR>";
        $stmt = $mysqli->prepare($this->query);

        $this->BindWhere($stmt);

        $stmt->execute();

        $data = mysqli_stmt_result_metadata($stmt);

        $fields = array();
        $out = array();

        $fields[0] = $stmt;

        while ($field = mysqli_fetch_field($data)) {
            $fields[] =& $out[$field->name];
        }

        call_user_func_array('mysqli_stmt_bind_result', $fields);

        $results = array();

        while ($stmt->fetch()) {
            $row = array();
            foreach ($out as $key => $value) {
                $row[$key] = $value;
            }
            $results[] = $row;
        }

        return $results;
    }

    public function GetBlankTableRow($table)
    {
        return array();
        if (array_key_exists($table, $this->blankrows)) {
            return $this->blankrows[$table];
        }
        $this->SetTable($table);
        $this->AddField('*');
        $this->AddLimit(1);
        $q_row = $this->GetSingleRow();

        $output = array();
        foreach ($q_row as $field => $value) {
            if (is_int($value) || $value == '0') {
                $output[$field] = 0;
            } else {
                $output[$field] = '';
            }
        }
        $this->blankrows[$table] = $output;
        return $output;
    }

    public function InsertRow($table, $pri_key, &$row)
    {
        global $mysqli;

        $query = "INSERT INTO $table (";
        $fieldnames = array();
        $qmarks = array();

        $params = array();
        $params[0] = '';
        $params[1] = '';

        foreach ($row as $field => $value) {
            if ($field != $pri_key && $value !== '') {
                $fieldnames[] = $field;

                // Params to mysqli_stmt_bind_param now need to be reference
                $params[] =& $row[$field];

                $qmarks[] = '?';
                if (is_int($value)) {
                    $params[1] .= 'i';
                } elseif (is_numeric($value)) {
                    $params[1] .= 'd';
                } else {
                    $params[1] .= 's';
                }
            }
        }
        $query .= implode(',', $fieldnames);
        $query .= ') VALUES (';
        $query .= implode(',', $qmarks);
        $query .= ')';
        $stmt = $mysqli->prepare($query);
        if ($mysqli->error) {
            echo $string['showerror'] . '<br >';
            exit();
        }
        $params[0] = $stmt;
        call_user_func_array('mysqli_stmt_bind_param', $params);
        $stmt->execute();

        $row[$pri_key] = $stmt->insert_id;
    }
}
