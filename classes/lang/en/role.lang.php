<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The localised role names.
 *
 * @author Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright Copyright (c) 2020 The University of Nottingham
 * @package core
 */

$string['admin'] = 'Admin';
$string['externalexaminer'] = 'External Examiner';
$string['graduate'] = 'Graduate';
$string['invigilator'] = 'Invigilator';
$string['internalreviewer'] = 'Internal Reviewer';
$string['inactivestaff'] = 'Inactive Staff';
$string['left'] = 'Left University';
$string['locked'] = 'Locked';
$string['staff'] = 'Staff';
$string['standardssetter'] = 'Standards Setter';
$string['student'] = 'Student';
$string['suspended'] = 'Suspended';
$string['sysadmin'] = 'SysAdmin';
$string['syscron'] = 'System CRON';
