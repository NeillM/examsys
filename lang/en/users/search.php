<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/user_search_options.php';
require '../lang/' . $language . '/include/titles.php';

$string['usermanagement'] = 'User Management';
$string['usersearch'] = 'User Search';
$string['performsummary'] = 'Performance Summary';
$string['msg1'] = "<strong>Warning:</strong>&nbsp;&nbsp;You have not ticked any categories under 'Advanced' to search for.";
$string['msg2'] = 'No users found for specified search criteria';
$string['largeresult'] = 'Large number of users found, only the first 10,000 are displayed.'
    . ' Please try narrowing your search criteria.';
$string['to'] = 'to';
$string['of'] = 'of';
$string['nonstudent'] = 'You have selected a non-student user.';
