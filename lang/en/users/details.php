<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/user_search_options.php';
require '../lang/' . $language . '/include/titles.php';

$string['usermanagement'] = 'User Management';
$string['usersearch'] = 'User Search';
$string['user'] = 'User:';
$string['edit'] = 'Edit';
$string['status'] = 'Status';
$string['student'] = 'Student';
$string['Students'] = 'Students';
$string['standardssetter'] = 'Standards Setter';
$string['externalexaminer'] = 'External Examiner';
$string['internalreviewer'] = 'Internal Reviewer';
$string['graduate'] = 'Graduate';
$string['left'] = 'Left University';
$string['suspended'] = 'Suspended';
$string['locked'] = 'Locked';
$string['Staff'] = 'Staff';
$string['externalauth'] = '[Using external auth]';
$string['gender'] = 'Gender';
$string['male'] = 'Male';
$string['female'] = 'Female';
$string['other'] = 'Other';
$string['year'] = 'Year';
$string['yearofstudy'] = 'Year of Study';
$string['course'] = 'Course';
$string['userid'] = 'ExamSys User ID';
$string['log'] = 'Log';
$string['teams'] = 'Teams';
$string['team'] = 'Team';
$string['modules'] = 'Modules';
$string['notes'] = 'Notes';
$string['admin'] = 'Admin';
$string['accessibility'] = 'Accessibility';
$string['metadata'] = 'Metadata';
$string['papername'] = 'Paper Name';
$string['type'] = 'Type';
$string['started'] = 'Started';
$string['duration'] = 'Duration';
$string['ipaddress'] = 'IP Address';
$string['update'] = 'Update';
$string['telephone'] = 'Telephone';
$string['ldapunavailable'] = 'LDAP Server Unavailable';
$string['unknown'] = '&lt;unknown&gt;';
$string['password'] = 'Password';
$string['email'] = 'Email';
$string['formative'] = 'Formative';
$string['progresstest'] = 'Progress Test';
$string['summative'] = 'Summative Exam';
$string['survey'] = 'Survey';
$string['oscestation'] = 'OSCE Station';
$string['offlinepaper'] = 'Offline Paper';
$string['peerreview'] = 'Peer Review';
$string['noassessmentstaken'] = '&lt;no assessments/surveys taken&gt;';
$string['moduleid'] = 'Module ID';
$string['name'] = 'Name';
$string['academicyear'] = 'Academic Year';
$string['date'] = 'Date';
$string['paper'] = 'Paper';
$string['note'] = 'Note';
$string['author'] = 'Author';
$string['newnote'] = 'New Note';
$string['extratime'] = 'Extra Time';
$string['fontsize'] = 'Font Size';
$string['typeface'] = 'Typeface';
$string['background'] = 'Background Colour';
$string['foreground'] = 'Foreground Colour';
$string['markscolour'] = 'Marks Colour';
$string['themecolour'] = 'Heading/Theme Colour';
$string['labelscolour'] = 'Note/Labels Colour';
$string['unanswered'] = 'Unanswered/Error';
$string['dismisscolor'] = 'Dismiss Color';
$string['medical'] = 'Medical';
$string['breaks'] = 'Breaks';
$string['globalthemecolour'] = 'Global Theme Colour';
$string['globalthemefontcolour'] = 'Global Theme Font Colour';
$string['highlightcolour'] = 'Highlight Colour';
$string['default'] = 'Default';
$string['noextratime'] = 'no extra time';
$string['angledefault'] = '<default>';
$string['value'] = 'Value';
$string['save'] = 'Save';
$string['dateadded'] = 'Date Added';
$string['editteams'] = 'Edit Teams...';
$string['editmodules'] = 'Edit Modules...';
$string['system'] = 'System';
$string['na'] = 'N/A';
$string['universitylecturer'] = 'University Lecturer';
$string['universitylibrarian'] = 'University Librarian';
$string['universityadmin'] = 'University Admin';
$string['universitytechnical'] = 'University IT/Technical';
$string['nhslecturer'] = 'NHS Lecturer/Consultant';
$string['nhsadmin'] = 'NHS Admin';
$string['invigilator'] = 'Invigilator';
$string['inactivestaff'] = 'Inactive Staff';
$string['sysadmin'] = 'SysAdmin';
$string['reset'] = 'Reset';
$string['forcereset'] = 'Force Reset';
$string['classifiedinfo'] = 'classified information';
$string['custom'] = 'Custom';
$string['Access Denied'] = 'Access Denied';
$string['resitcandidate'] = 'Resit candidate';
$string['roles'] = 'Roles';
$string['rolesavefailure'] = 'The roles could not be updated.';
$string['breaktime'] = 'Break Time (percent)';
$string['breaktimeminpserhour'] = 'Break Time (mins per hour)';
$string['nobreaktime'] = 'no break time';
$string['profileaudit'] = 'Profile Audit';

//demo
$string['demo1'] = 'Cities';
$string['demo2'] = 'Which of the following are European cities?';
$string['demo3'] = 'True';
$string['demo4'] = 'False';
$string['demo5'] = 'London';
$string['demo6'] = 'New York';
$string['demo7'] = 'Paris';
$string['demo8'] = '3 marks';
$string['demo9'] = 'This is a note';
$string['demo10'] = 'Example paper';
$string['demo11'] = '© 2021, University of Nottingham ';
$string['demoblurb'] = 'Below is ane example of what an exam screen will look like with the settings applied.';

//Colour picker
$string['colour'] = 'Colour';
$string['themecolours'] = 'Theme Colours';
$string['standardcolours'] = 'Standard Colours';
$string['more'] = 'More...';

//Profile Audit
$string['changedtime'] = 'Date/Time';
$string['setting'] = 'Profile Setting';
$string['old'] = 'Old value';
$string['new'] = 'New Value';
$string['changedby'] = 'Changed by';
$string['background'] = 'Background';
$string['foreground'] = 'Foreground';
$string['marks'] = 'Marks';
$string['theme'] = 'Theme';
$string['label'] = 'Label';
$string['unanswered'] = 'Unanswered';
$string['dismiss'] = 'Dismiss';
$string['globaltheme'] = 'Global Theme';
$string['globalthemefont'] = 'Global theme Typeface';
$string['highlight'] = 'Highlight';
$string['textsize'] = 'Text size';
$string['font'] = 'Typeface';
$string['selectcolour'] = 'Select a %s';
$string['resettodefault'] = 'Reset %s to system default';
