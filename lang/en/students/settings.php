<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

$string['title'] = 'User Profile';
$string['examsettings'] = 'Exam Settings';
$string['systemsettings'] = 'System Settings';
$string['fontsize'] = 'Font Size';
$string['typeface'] = 'Typeface';
$string['background'] = 'Paper Background Colour';
$string['foreground'] = 'Main Text Colour';
$string['markscolour'] = 'Question Marks Text Colour';
$string['themecolour'] = 'Question Heading Text Colour';
$string['labelscolour'] = 'Question Labels Text Colour';
$string['unanswered'] = 'Unanswered Question Highlight Colour';
$string['dismisscolor'] = 'Dismiss Option Text Color';
$string['globalthemecolour'] = 'Exam Header/Footer Background Colour';
$string['globalthemefontcolour'] = 'Exam Header/Footer Text Colour';
$string['highlightcolour'] = 'Highlighted Question/Option Colour';
$string['default'] = 'Default';
$string['angledefault'] = '<default>';
$string['save'] = 'Save';
$string['demo1'] = 'Cities';
$string['demo2'] = 'Which of the following are European cities?';
$string['demo3'] = 'True';
$string['demo4'] = 'False';
$string['demo5'] = 'London';
$string['demo6'] = 'New York';
$string['demo7'] = 'Paris';
$string['demo8'] = '3 marks';
$string['demo9'] = 'This is a note';
$string['demo10'] = 'Example paper';
$string['demo11'] = '© 2021, University of Nottingham';
$string['colour'] = 'Colour';
$string['themecolours'] = 'Theme Colours';
$string['standardcolours'] = 'Standard Colours';
$string['more'] = 'More...';
$string['close'] = 'Close';
$string['eassessmentmanagementsystem'] = 'eAssessment Management System';
$string['resetdefault'] = 'Reset to Default';
$string['resetprevious'] = 'Undo';
$string['demoblurb'] = 'Below is ane example of what an exam screen will look like with the settings applied.';
$string['systemsettingsblrub'] = 'These settings are applied to the students landing page, feedback, user profile, exam landing pages and in exams.';
$string['examsettingsblrub'] = 'These settings are only applied in exams.';
$string['menu'] = 'Menu';
$string['settingswarning'] = 'Settings will only be applied to your profile after you have pressed "Save".';
$string['username'] = 'Username';
$string['system'] = 'ExamSys';
$string['selectcolour'] = 'Select a %s';
$string['resettodefault'] = 'Reset %s to system default';
$string['true'] = 'True';
$string['false'] = 'False';
