<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

$string['hotspots'] = 'Hotspots';
$string['correctAnswers'] = 'Correct Answers';
$string['incorrectAnswers'] = 'Incorrect Answers';
$string['errorcanvas'] = 'Canvas not supported';
$string['errorimageshotspot'] = 'Hotspot question cannot be displayed because some images were not loaded.';
$string['hotspotcorrection'] = 'The green dots show students answers which will be marked as correct if saved.';
$string['image'] = 'Image';
$string['correctionmode'] = 'Correction mode';
$string['correctlayers'] = 'Correct Layer(s)';
$string['incorrectlayers'] = 'Incorrect Layer(s)';
$string['hotspot_letters_blank_note'] = 'NOTE: Please ensure you mark all answer areas you wish to be marked with letters, leaving the label blank for non-answers. These are invisible to students';
