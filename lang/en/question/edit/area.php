<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

$string['But_Clear_All'] = 'Clear All';
$string['But_Delete_point'] = 'Delete point';
$string['But_your_answer'] = 'Your answer';
$string['But_correct_answer'] = 'Correct answer';
$string['But_show_error'] = 'Show error';
$string['Magnify'] = 'Magnify';
$string['tt_Clear_All'] = 'Clear All';
$string['tt_Delete_point'] = 'Delete point';
$string['tt_your_answer'] = 'Your answer';
$string['tt_correct_answer'] = 'Correct answer';
$string['tt_show_error'] = 'Show error';
$string['tt_Magnify'] = 'Magnify';
$string['Start_over'] = 'Start over';
$string['Eraser'] = 'Eraser';
$string['popUp_msg'] = 'Do you want to clear all lines?';
$string['popUp_yes'] = 'Yes';
$string['popUp_no'] = 'No';
$string['errorcanvas'] = 'Canvas not supported';
$string['errorimagesarea'] = 'area question cannot be displayed because some images were not loaded.';
