<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

$string['accessibility'] = 'Accessibility';
$string['extratime'] = 'Extra time';
$string['backgroundcolour'] = 'Background colour';
$string['foregroundcolour'] = 'Foreground colour';
$string['markscolour'] = 'Marks colour';
$string['themecolour'] = 'Theme/Heading colour';
$string['labelcolour'] = 'Label colour';
$string['unansweredbackground'] = 'Unanswered background';
$string['questiondismiss'] = 'Question dismiss';
$string['fontsize'] = 'Font size';
$string['typeface'] = 'Typeface';
$string['breaktime'] = 'Break Time (percent)';
$string['breaktimeminpserhour'] = 'Break Time (mins per hour)';
