<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

$string['qualitativeanalysis'] = 'Qualitative Analysis';
$string['highlight'] = 'Highlight';
$string['collapse'] = 'Collapse';
$string['casesensitive'] = 'Case-sensitive';
$string['screen'] = 'Screen';
$string['comments'] = '%d comments.';
$string['nocomments'] = '&lt;No Comments&gt;';
$string['occurencesof'] = '%d - occurrences of <strong>%s</strong> in %d comments.';
$string['period'] = 'Quanlitative report for survey taken between %s and %s.';
