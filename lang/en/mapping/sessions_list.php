<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

$string['sessions'] = 'Sessions';
$string['createsession'] = 'Create new Session';
$string['importnle'] = 'Import Sessions from %s';
$string['importfile'] = 'Import Sessions from File';
$string['editsession'] = 'Edit Session';
$string['deletesession'] = 'Delete Session';
$string['copyyear'] = 'Copy Year';
$string['date'] = 'Date';
$string['name'] = 'Name';
$string['objectives'] = 'Objectives';
$string['manageobjectives'] = 'Manage Objectives';
$string['msg1'] = 'Source and destination years must be different.';
$string['msg2'] = 'Please select years to copy from and to.';
$string['msg3'] = 'Invalid value in year list';
$string['msg4'] = 'No years available to copy';
$string['msg5'] = 'Copy objectives from';
$string['msg6'] = 'Copy objectives to';
$string['copy'] = 'Copy';
$string['vlemapping'] = 'This is %s-based module. To add a new session it must be added in the %s';
$string['vlemappingedit'] = 'This is %s-based module. To change its session objectives you must edit the %s';
$string['vlemappingdelete'] = 'This is %s-based module.To delete this session it must be removed from the %s';
