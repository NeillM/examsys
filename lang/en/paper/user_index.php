<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/include/paper_security.php';

$string['startscreen'] = 'Start Screen';
$string['availability'] = 'Availability:';
$string['to'] = 'to';
$string['candidates'] = 'Candidates:';
$string['screens'] = 'Screens:';
$string['marks'] = 'Marks:';
$string['adjusted'] = 'Adjusted using random mark of ';
$string['currentuser'] = 'Current User:';
$string['timeremaining'] = 'Time remaining:';
$string['navigation'] = 'Navigation:';
$string['bidirectional'] = 'Bidirectional';
$string['unidirectional'] = 'Unidirectional';
$string['start'] = 'Start';
$string['restart'] = 'Re-Start';
$string['previouscompletions'] = 'Previous Completions';
$string['rubric'] = 'Rubric:';
$string['duration'] = 'Duration:';
$string['testclip'] = 'Test Clip';
$string['switchpapers'] = 'Switch Papers';
$string['papernotavailable'] = 'Paper not available at this time.';
$string['papernotavailablestudents'] = 'Paper not available to students at this time.';
$string['nottakenpaper'] = '(You have not yet taken this paper.)';
$string['donotstart'] = "Do not click 'Start' until instructed to by an invigilator.";
$string['hour'] = 'hour';
$string['hours'] = 'hours';
$string['minutes'] = 'minutes';
$string['mins'] = 'mins';
$string['notset'] = 'Not set';
$string['secs'] = 'secs';
$string['papernotfound'] = 'Paper not Found';
$string['requestedpaper'] = 'The requested paper cannot be found.';
$string['guestaccount'] = 'Guest Account';
$string['metadata_msg'] = "Your '%s' does not match '%s'.";
$string['timeexpired'] = 'Remaining time has now expired.';
$string['tooltip_bidirectional'] = 'You are allowed to move both backwards and forwards between screens.';
$string['tooltip_unidirectional'] = 'Once you have submitted a screen you will not be able to move back.';
$string['tooltip_adjustmark'] = 'Your percentage will be automatically adjusted post-exam to account for the random mark someone could get by guessing at random. You are therefore advised to answer all questions on the paper.';
$string['tooltip_testclip'] = 'This is a test audio clip for you to test your headphones and volume are set correctly.';
$string['photoid'] = 'Photo ID';
$string['ok'] = 'OK';
$string['ipmismatchtitle'] = 'You appear to be logged into this exam on another device.';
$string['ipmismatchblurb'] = 'This device will now assume control of the exam. Notify the invigilator if you are unsure how to proceed.';
$string['remoteipmismatchblurb'] = 'Your exam attempt will resume on this device.';
$string['waitforpassword'] = 'You may start once you have received the exam password.';
$string['logissue'] = 'Log an issue that stops you completing the exam';
$string['sebrequired'] = 'This paper must be accessed via the Safe Exam Browser.';
$string['menu'] = 'Menu';
$string['metadata'] = 'Metadata:';
