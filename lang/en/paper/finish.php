<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

require_once dirname(__DIR__, 3) . '/lang/' . $language . '/include/paper_security.php';
require_once dirname(__DIR__, 3) . '/lang/' . $language . '/question/sct_shared.php';

$string['question'] = 'Question';
$string['norights'] = 'You do not have rights to see this paper.';
$string['examscript'] = 'Exam Script';
$string['error_paper'] = 'The requested paper cannot be found.';
$string['thankyoumsg'] = 'Thank you for completing <strong>%s</strong>. Your responses have been recorded.';
$string['studentviewend'] = 'Student view ends here';
$string['staffviewbelow'] = '<strong>Staff only view below here </strong>(students will not see this)';
$string['answersscreen'] = 'Answers Screen for';
$string['key'] = 'Key:';
$string['correctanswer'] = 'Correct answer';
$string['incorrectanswer'] = 'Incorrect answer';
$string['boldwords'] = "<strong>Emboldened</strong> words represent the correct response for each question (not the user's answer).";
$string['feedbackinred'] = 'Feedback is displayed in dark red italics';
$string['note'] = 'Note';
$string['mousereveal'] = 'Move the mouse over incorrect labels to reveal the correct answer';
$string['screen'] = 'Screen %s of %s';
$string['unanswered'] = 'unanswered';
$string['summaryofmarks'] = 'Summary of Marks';
$string['yourmark'] = 'Your mark';
$string['randommark'] = 'Random mark';
$string['passmark'] = 'Pass Mark';
$string['yourpercentage'] = 'Your percentage';
$string['adjusted'] = '(adjusted)';
$string['feedback'] = 'Feedback';
$string['msg1'] = 'Thank you for completing <strong>%s</strong>. Your responses have been recorded.';
$string['msg2'] = 'To log out please click “Close window” and then press CTRL, ALT and DELETE to log out of your workstation.';
$string['msg2short'] = 'Please press CTRL, ALT and DELETE to log out of your workstation.';
$string['msg2remote'] = 'Please click here to close this window and logout of ExamSys.';
$string['closewindow'] = 'Close Window';
$string['overallcorrectorder'] = 'Overall correct order (Bonus Mark)';
$string['outof'] = 'out of';
$string['learningobjectives'] = 'Learning Objectives';
$string['experimentalquestion'] = '0 - Experimental Question';
$string['unmarked'] = 'unmarked';
$string['tdiagnosis'] = 'diagnosis';
$string['tinvestigation'] = 'investigation';
$string['tprescription'] = 'prescription';
$string['tintervention'] = 'intervention';
$string['ttreatment'] = 'treatment';
$string['thankyou'] = 'Thank you';
$string['difficultyofthequestion'] = 'difficulty of the question (i.e. standards set). Roll over for full category title.';
$string['withatoleranceof'] = 'within a tolerance of';
$string['true'] = 'True';
$string['false'] = 'False';
$string['yes'] = 'Yes';
$string['no'] = 'No';
$string['abstain'] = 'Abstain';
$string['iscorrect'] = 'Is correct';
$string['isexcluded'] = 'Is excluded';
$string['withinshape'] = 'Within shape';
$string['outsideshape'] = 'Outside shape';
$string['useranswererror'] = 'User Answer Error';
$string['errorkeywordunique'] = '<strong>ERROR:</strong> unable to find unique question for supplied keywords.';
$string['errorrandomnotfound'] = '<strong>ERROR:</strong> No random question selected. Perhaps this screen was skipped.';
$string['overriddenby'] = 'Mark adjusted by';
$string['questionclarification'] = 'Question Clarification';
$string['EnhancedCalcCorrectError'] = 'Error: Correct answer could not be calculated';
$string['student'] = 'Student';
$string['started'] = 'Started';
$string['finished'] = 'Finished';
$string['comments'] = 'Comments:';
$string['logissue'] = 'Log an issue you had during the exam';
$string['previewmathjax'] = 'Preview';
$string['postscript'] = 'Postscript';
$string['closeexam'] = 'Close exam instructions';
$string['print'] = 'Print Feedback';
