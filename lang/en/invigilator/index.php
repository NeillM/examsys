<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

$string['invigilatoraccess'] = 'Invigilator Access';
$string['lab'] = 'Lab:';
$string['unknownlab'] = ' - unknown lab';
$string['nopapersfound'] = 'No papers found!';
$string['nopapersfoundmsg'] = 'No assessments can be found for the current lab at this time.';
$string['emergencynumbers'] = 'Emergency Numbers';
$string['prefix'] = 'Title';
$string['surname'] = 'Surname';
$string['forenames'] = 'Forenames';
$string['extension_mins'] = 'Extra (mins)';
$string['endtime'] = 'End';
$string['extendtime'] = 'Extend Time';
$string['extendtimeby'] = 'Extend Time By';
$string['addnote'] = 'Add note';
$string['toiletbreak'] = 'Rest Break';
$string['unfinished'] = 'Set to unfinished';
$string['currenttime']    = 'Current Time';
$string['start']     = 'Start';
$string['end']       = 'End';
$string['start_but'] = 'Start';
$string['endat_but']  = 'End At';
$string['session_end'] = 'Session End';
$string['duration']  = 'Duration';
$string['mins']      = 'mins';
$string['hour']      = 'hour';
$string['hours']     = 'hours';
$string['papernote'] = 'Add Paper Note...';
$string['extratime'] = 'Extra Time';
$string['preexam'] = 'Pre-Exam';
$string['preexamlist'] = '<ol>
    <li>Place log in instructions at each workstation</li>
    <li>Place blank paper each workstation</li>
    <li>Check all students have logged in correctly</li>
    <li>Use \'Guest Login\' accounts for anyone not able to log in</li>
    <li><strong>NOTE:</strong> Do not start before scheduled start time</li>
    </ol>';
$string['remotepreexamlist'] = '<ol>
    <li>Confirm all students have the exam password</li>
    <li>If a user cannot start the exam ask them to provide evidence via the log issue link</li>
    </ol>';
$string['midexam'] = 'Mid-Exam';
$string['midexamlist'] = '<ol>
    <li>For emergency support call one of the numbers show on the paper tabs</li>
    <li>Record IT/personal problems against relevant students (click their name)</li>
    <li>Record rest breaks against relevant students (click their name)</li>
    <li>Record general paper/question problems using \'Add Paper Note...\' button.</li>
    </ol>';
$string['remotemidexamlist'] = '<ol>
    <li>For emergency support call one of the numbers show on the paper tabs</li>
    <li>Record IT/personal problems against relevant students (click their name)</li>
    <li>Record general paper/question problems using \'Add Paper Note...\' button.</li>
    </ol>';
$string['postexam'] = 'Post-Exam';
$string['postexamlist'] = '<ol>
    <li><img src="../artwork/green_speech.gif" width="15" height="14" /> "That is the end of the exam. Please navigate to the last screen and click \'Finish\'."</li>
    <li><img src="../artwork/green_speech.gif" width="15" height="14" /> "Click \'Close Window\' and then log out of Rog&#333."</li>
    <li>Collect up log in instructions for reuse</li>
    <li>Collect and dispose of blank paper</li>
    <li>Ensure <strong>all</strong> workstations are logged out</li>
    </ol>';
$string['remotepostexamlist'] = '<ol>
    <li>Ask users to provide evidence of any issues during the exam via the log issue link.</li>
    </ol>';
$string['time'] = 'Time';
$string['timedexam'] = 'Timed Exam:';
$string['timeerror'] = 'Time Error';
$string['timeerrormsg'] = 'Exam must be at least %d minutes in length';
$string['examquestionclarifications'] = 'Exam question clarifications';
$string['midexamclarifications'] = 'Mid-Exam Clarifications';
$string['examchecklist'] = 'Exam Checklist';
$string['viewrubric'] = 'View Rubric';
$string['examrubric'] = 'Exam Rubric';
$string['unknowncomputer'] = 'Unknown Computer';
$string['unknowncomputermsg'] = 'The computer you are attempting to access ExamSys from is not recognised.<br />Please call one of the emergency numbers for help.';
$string['logout'] = 'Logout';
