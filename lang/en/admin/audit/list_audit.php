<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

$string['title'] = 'Access Audit';
$string['object'] = 'Object';
$string['objecttype'] = 'Object Type';
$string['time'] = 'Time';
$string['user'] = 'User';
$string['affecteduser'] = 'Affected User';
$string['source'] = 'Source';
$string['eventtype'] = 'Event Type';
$string['link'] = 'Settings';
$string['titlefromto'] = 'Access Audit (%s to %s of %s)';
$string['pagecount'] = 'Result page navigation';
$string['breadcrumb'] = 'Admin screen navigation';
