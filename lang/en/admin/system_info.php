<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

$string['systeminformation'] = 'System Information';
$string['table'] = 'Table';
$string['records'] = 'Records';
$string['updated'] = 'Updated';
$string['application'] = 'ExamSys application';
$string['rogoplugins'] = 'ExamSys plug-ins';
$string['version'] = 'Version';
$string['webroot'] = 'Web Root';
$string['database'] = 'Database';
$string['authentication'] = 'Authentication';
$string['Session'] = 'Session';
$string['serverinformation'] = 'Server Information';
$string['processor'] = 'Processor';
$string['cpus'] = 'CPUs';
$string['cores'] = 'Cores';
$string['servername'] = 'Server name';
$string['hostname'] = 'Host name';
$string['ipaddress'] = 'IP Address';
$string['clock'] = 'Clock';
$string['os'] = 'OS';
$string['apache'] = 'Apache';
$string['php'] = 'PHP';
$string['mysql'] = 'MySQL';
$string['partitions'] = 'Partitions';
$string['mysqlstatus'] = 'MySQL Status';
$string['uptime'] = 'Uptime';
$string['minutes'] = 'minutes';
$string['hours'] = 'hours';
$string['days'] = 'days';
$string['threads'] = 'Threads';
$string['questions'] = 'Questions';
$string['slow queries'] = 'Slow queries';
$string['opens'] = 'Opens';
$string['flush tables'] = 'Flush tables';
$string['open tables'] = 'Open tables';
$string['queries per second avg'] = 'Queries per second avg';
$string['na'] = 'N/A';
$string['driveicon'] = 'Drive icon';
$string['freespace'] = '%s free of %s';
$string['more_details'] = 'More details...';
$string['lookups'] = 'Lookups';
$string['EnhancedCalcPlugin'] = 'Calculation question';
$string['ErrorLogSettings'] = 'Error Reporting';
$string['authdebug'] = 'Show authentication debug';
$string['errorsonscreen'] = 'Show errors on-screen';
$string['phpnotices'] = 'Show PHP Notices';
$string['errorshutdown'] = 'Error handling at shutdown';
$string['varcapturemethod'] = 'Variable capture method on error';
$string['improved'] = 'Improved';
$string['basic'] = 'Basic';
$string['none'] = 'none';
$string['company'] = 'Company';
$string['webserver'] = 'Web server';
$string['errorslogged'] = 'Errors logged to file';
$string['build'] = 'Build';
$string['on'] = 'On';
$string['off'] = 'Off';
$string['defaultcalculations'] = 'BLANK therefore phpEval';
$string['type'] = 'Type';
$string['setting'] = 'Setting';
$string['value'] = 'Value';
$string['directories'] = 'Directories';
$string['dir_media'] = 'Media';
$string['dir_theme'] = 'Theme';
$string['dir_student_help'] = 'Student help';
$string['dir_staff_help'] = 'Staff help';
$string['dir_photo'] = 'Photos';
$string['dir_qti_import'] = 'QTi import';
$string['dir_qti_export'] = 'QTi export';
$string['dir_email'] = 'E-mail templates';
$string['permissions_good'] = 'Permissions correct';
$string['permissions_bad'] = 'Permissions incorrect';
