<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

$string['clearguestaccounts'] = 'Clear Guest Accounts';
$string['clear'] = 'Clear';
$string['user'] = 'User';
$string['password'] = 'Password';
$string['surname'] = 'Surname';
$string['firstnames'] = 'First Names';
$string['title'] = 'Title';
$string['studentid'] = 'Student ID';
$string['datereserved'] = 'Date Account Reserved';
$string['assessmenttaken'] = 'Assessment Taken';
$string['free'] = 'free';
$string['not taken'] = 'not taken';
$string['cleanup'] = 'Clean Up';
$string['unset'] = '&lt;unset&gt;';
$string['reservedwarning'] = 'Are you sure you want to clear this account? It was reserved under 15 minutes ago.';
