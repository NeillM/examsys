<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

$string['gender'] = 'Gender';
$string['title'] = 'Title';
$string['surname'] = 'Surname';
$string['firstnames'] = 'First Names';
$string['studentid'] = 'Student ID';
$string['course'] = 'Course';
$string['year'] = 'Year';
$string['submitted'] = 'Submitted';
$string['started'] = 'Started';
$string['correctanswers'] = 'Correct answers ->';
$string['nodata'] = 'No students took the exam in the selected date range';
$string['bonus'] = 'bonus';
