<?php

// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

/**
 * SMS plugin functions
 * @author Dr Joseph Baxter <joseph.baxter@nottingham.ac.uk>
 * @copyright Copyright (c) 2016 onwards The University of Nottingham
 */

namespace plugins;

/**
 * Abstract SMS class.
 *
 * This class should be extend by classes used define sms plugins.
 */
abstract class plugins_sms extends \plugins\plugins
{
    /**
     * Type of the plugin.
     * @var string
     */
    protected $plugin_type = 'SMS';
    /**
     * Install steps for sms plugin.
     * @return bool true on success
     */
    protected function type_install()
    {
        // Add external system to external systems table
        $external = new \external_systems();
        return $external->insert_external_system($this::SMS, \external_systems::PLUGIN);
    }
    /**
     * Uninstall steps for sms plugin.
     * @return bool true on success
     */
    protected function type_uninstall()
    {
        $success = true;
        // Do not remove external systems if in use.
        $checksql = $this->db->prepare('SELECT NULL FROM faculty WHERE externalsys = ?
            UNION
                SELECT NULL FROM courses WHERE externalsys = ?
            UNION
                SELECT NULL FROM schools WHERE externalsys = ?
            UNION
                SELECT NULL FROM modules WHERE sms = ?
            UNION
                SELECT NULL FROM properties WHERE externalsys = ? LIMIT 1');
        $pluginname = '\\plugins\\SMS\\' . $this->plugin . '\\' . $this->plugin;
        $name = $pluginname::SMS;
        $checksql->bind_param('sssss', $name, $name, $name, $name, $name);
        $checksql->execute();
        $checksql->store_result();
        if ($checksql->num_rows != 1) {
            // Remove external system from external systems table
            $insertsql = $this->db->prepare('DELETE IGNORE FROM external_systems WHERE name = ? AND type = ?');
            $plugin = \external_systems::PLUGIN;
            $insertsql->bind_param('ss', $name, $plugin);
            $insertsql->execute();
            $insertsql->close();
            if ($this->db->errno != 0) {
                $success = false;
            }
        }
        $checksql->close();
        return $success;
    }

    /**
     * Render module sync options
     * @param array $moduleimport module import data from plugin
     * @param integer $moduleid internal id for module
     * @param string $externalid external id for module
     * @return void
     */
    public static function render_module_sync_options($moduleimport, $moduleid, $externalid)
    {
        $configObject = \Config::get_instance();
        $yearutils = new \yearutils($configObject->db);
        if ($moduleid === 0) {
            $module_year_start = '';
        } else {
            $module_year_start = \module_utils::getAcademicYearStart($moduleid);
        }
        $data['current_session'] = $yearutils->get_current_session($module_year_start);
        $data['next_session'] = $yearutils->get_next_session($module_year_start);
        $data['previous_session'] = $yearutils->get_previous_session($module_year_start);
        $data['academic_year'] = $yearutils->get_academic_session($data['current_session']);
        $data['next_academic_year'] = $yearutils->get_academic_session($data['next_session']);
        $data['prev_academic_year'] = $yearutils->get_academic_session($data['previous_session']);
        $data['prevenabled'] = false;
        $data['url'] = $moduleimport['url'];
        $data['tooltip'] = $moduleimport['tooltip'];
        $data['blurb'] = $moduleimport['blurb'];
        $data['externalid'] = $externalid;
        if (\module_utils::check_sync_previous_year($moduleid)) {
            $data['prevenabled'] = true;
        }
        $render = new \render($configObject);
        $render->render($data, array(), 'module/syncoptions.html');
    }

    /**
     * Enable this plugin.
     */
    public function enable_plugin()
    {
        $enabled = $this->config->get_setting('plugin_SMS', 'enabled_plugin');
        if (!is_null($enabled)) {
            if (is_array($enabled)) {
                if (!in_array($this->plugin, $enabled)) {
                    $enabled[] = $this->plugin;
                }
            } else {
                $enabled = array($enabled, $this->plugin);
            }
        } else {
            $enabled = array($this->plugin);
        }
        $this->config->set_setting('enabled_plugin', $enabled, \Config::JSON, 'plugin_SMS');
    }
    /**
     * Disable this plugin.
     */
    public function disable_plugin()
    {
        $enabled = $this->config->get_setting('plugin_SMS', 'enabled_plugin');
        if (!is_null($enabled)) {
            $newenabled = array();
            $key = array_search($this->plugin, $enabled);
            foreach ($enabled as $sms) {
                if ($key !== false and $this->plugin === $sms) {
                    continue;
                }
                $newenabled[] = $sms;
            }
            $this->config->set_setting('enabled_plugin', $newenabled, \Config::JSON, 'plugin_SMS');
        }
    }
    /**
     * Get all assessments for academic session
     * @params integer $session academic session to sync assessments with
     */
    abstract public function get_assessments($session);
    /**
     * Get all enrolments for academic session
     * @params integer $session academic session to sync enrolments with
     * @params integer $externalid external system module id
     */
    abstract public function get_enrolments($session = null, $externalid = null);
    /**
     * Update module in an academic session
     * Updates module details and enrolments
     * @params integer $externalid external system module id
     * @params integer $session academic session to sync enrolments with
     */
    abstract public function update_module_enrolments($externalid, $session);
    /**
     * Get faculties/schools.
     */
    abstract public function get_faculties();
    /**
     * Get courses.
     */
    abstract public function get_courses();
    /**
     * Get modules.
     * @params integer $externalid external system module id
     * @params integer $session academic session to sync enrolments with
     */
    abstract public function get_modules($externalid = null, $session = null);
    /**
     * Write a gradebook for an academic session to a file to be processed by campus solutions.
     * @param integer $session academic session to publish gradebook for
     */
    abstract public function publish_gradebook($session);
    /**
     * Check if module import is supported by the plugin
     * @return array|bool import url and translation strings, false if module import not supported
     */
    abstract public function supports_module_import();
    /**
     * Check if faculty/school import is supported by the plugin
     * @return array|bool import url and translation strings, false if faculty import not supported
     */
    abstract public function supports_faculty_import();
    /**
     * Check if course import is supported by the plugin
     * @return array|bool import url and translation strings, false if course import not supported
     */
    abstract public function supports_course_import();
    /**
     * Check if enorlment import is supported by the plugin
     * @return array|bool false if enrolment import not supported
     */
    abstract public function supports_enrol_import();
    /**
     * Check if assessment import is supported by the plugin
     * @return array|bool import url and translation strings, false if assessment import not supported
     */
    abstract public function supports_assessment_import();
    /**
     * Get name of sms
     * @return string name of sms
     */
    abstract public function get_name();
}
